from time import sleep

value = 0

while(1):
    sleep(0.1)
    with open('holder.bin', 'wb') as f:
        f.write(bytearray([value]))
        f.close()
        value = (value + 1) % 100
        print "Attention: " + str(value)