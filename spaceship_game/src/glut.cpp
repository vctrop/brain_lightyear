/*

*/

#include <GL/glut.h>
#include <GL/freeglut_ext.h>
#include <stdio.h>
#include <stdlib.h>

#include <shapes3D_gl.hpp>
#include <components.hpp>
#include <frames.h>
#include <point.h>
#include <brain_lightyear.hpp>

#define WIN_WIDTH 1280
#define WIN_HEIGHT 720

Brain_lightyear game(WIN_WIDTH, WIN_HEIGHT);

/////////////////////////////////// GLUT OUTPUT CALLBACKS ///////////////////////////////////////
// Sets projection configs to render in 2D
void config_2D(){
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, WIN_WIDTH, 0, WIN_HEIGHT, 1, -1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);
}

void reshape (int w, int h) {
    glViewport (0, 0, (GLsizei)w, (GLsizei)h);

    // 3D projection
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity ();
    gluPerspective(60, (GLfloat)WIN_WIDTH/(GLfloat)WIN_HEIGHT, 1.0, 99999999.0);
    glMatrixMode(GL_MODELVIEW);

}

void render() {

    game.run();
    /*Frames frame;
    float FPS = frame.getFrames();
    char FPS_txt[15] = "FPS: ";*/

    // FPS
    /*strcat(FPS_txt, patch::to_string(FPS).c_str());
    glColor3f(0.0, 0.0,DAS
    text(1150, 700, FPS_txt);*/

}

void idle_display (void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    render();
    glutSwapBuffers();
}
/////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////// GLUT INPUT CALLBACKS ////////////////////////////////////////

void keyb_up(unsigned char key, int x, int y)
{
    //printf("\n Liberou Tecla: %d" , key);
}

void keyboard(unsigned char key, int x, int y)
{
    game.check_kb(key);
    printf("\nTecla: %d" , key);
}

void mouseFunc(int button, int state, int x, int y)
{
    printf("\nMouse pressionado: botao %d  estado %d  (%d %d)", button, state, x, y);
}

void mouseMotionFunc(int x, int y)
{
   //printf("\nMouse moveu: %d %d", x, y);
   mouseFunc(-1, -1, x, y);
}

void mouseWheelCB(int wheel, int direction, int x, int y)
{
    //eng.wheel_check(wheel, direction, x, y);
	printf("\nMouseWheel: %d %d \n", x, y);
}

void entryFunc(int state)
{
   printf("\nMouse %d ", state);
}
/////////////////////////////////////////////////////////////////////////////////////////////////
/* Defines the lighting configurations */
void config_lighting(){
    GLfloat light_0_position[] = { 500, 50, -200, 0};
    GLfloat light_0_ambient[]  = { 0, 0, 0, 1};
    GLfloat light_0_difuse[]   = { 1, 1, 1, 1};
    GLfloat light_0_specular[] = { 1, 0.3, 0.3, 1};

    GLfloat light_1_position[] = { 0, 0, 200, 0};
    GLfloat light_1_ambient[]  = { 0, 0, 0, 1};
    GLfloat light_1_difuse[]   = { 1, 1, 1, 1};
    GLfloat light_1_specular[] = { 1, 1, 1, 1};

    GLfloat global_ambient[] = {0.4, 0.4, 0.4, 1};
    GLfloat materials_specular[] = {1, 1, 1, 1};
    GLfloat materials_shininess[] = {100};
    glEnable(GL_LIGHTING);

    glLightModelfv(GL_AMBIENT, global_ambient);
    glMaterialfv(GL_FRONT, GL_SPECULAR, materials_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, materials_shininess);

    glLightfv(GL_LIGHT0, GL_POSITION, light_0_position);
    glLightfv(GL_LIGHT0, GL_AMBIENT, light_0_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_0_difuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_0_specular);
    glEnable(GL_LIGHT0);

    glLightfv(GL_LIGHT1, GL_POSITION, light_1_position);
    glLightfv(GL_LIGHT1, GL_AMBIENT, light_1_ambient);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, light_1_difuse);
    glLightfv(GL_LIGHT1, GL_SPECULAR, light_1_specular);
    //glEnable(GL_LIGHT1);

    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);             // lets ambient and diffuse to be set by glColor

}

// Sets the glut configs and callbacks
void glut_initial_settings()
{
	//glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize (WIN_WIDTH, WIN_HEIGHT);
	glutInitWindowPosition (0, 0);
	glutCreateWindow ("Brain Lightyear - Victor O. Costa, Guilherme Retamal");
	config_lighting();

    // same of init()
	glClearColor(1,1,1,1);
	glPolygonMode(GL_FRONT, GL_FILL);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glutReshapeFunc(reshape);
	glutDisplayFunc(idle_display);

	glutKeyboardFunc(keyboard);
	glutKeyboardUpFunc(keyb_up);
	glutMotionFunc(mouseMotionFunc);
	glutMouseFunc(mouseFunc);
	glutEntryFunc(entryFunc);
    glutMouseWheelFunc(mouseWheelCB);

	glutIdleFunc(idle_display);
}

int main (int argc, char* argv[])
{
    // Calling glut functions
    glutInit(&argc, argv);
    glut_initial_settings();
	glutMainLoop();

	return 0;
}
