/*
Arquivo contendo a classe Transform_3D, que implementa uma s�rie de transformadas em tr�s dimens�es utilizando coordenadas cartesianas.
Utilizando essa classe, � poss�vel fazer a transi��o de coordenadas desde as coordenadas do modelo at� as coordenadas da tela.
Ao final do processo, � poss�vel obter apenas uma matriz para ser aplicada aos v�rtices.

Transforma��es implementadas:
(Model coord -> World coord)
- Translation;
- Scale;
- Rotation;
(World coord -> Eye coord)
- LookAt view;
(Eye coord -> Clip coord)
- Orthographic projection;
- Perspective projection;
(Clip coord -> Viewport)
- Normalization (ao multiplicar)
(Viewport -> Screen)
- viewport_to_screen;

A etapa de transi��o entre coordenadas Clip e Viewport (divis�o pelo termo homog�neo) � realizada ao multiplicar a transformada por um ponto. (m�todo operator)

Autor: Victor O. Costa
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <vector>

#include <point.h>

#define PI 3.14159265359

using std::vector;

#ifndef TRANSFORMS3D_H
#define TRANSFORMS3D_H

class Transform_3D
/* Implements various transforms in 3 dimensions */
    {
private:
    float transform_matrix[4][4];

public:
    Transform_3D(){
        load_identity();
    }

    /* Overwrites transform_matrix with the identity matrix */
    void load_identity(){
        int i, j;

        for(i = 0; i < 4; i++)
            for(j = 0; j < 4; j++){
                if(i == j)
                    transform_matrix[i][j] = 1;
                else
                    transform_matrix[i][j] = 0;
            }
    }

    /* Multiplies the current transform matrix by a translation matrix */
    void concat_translation(float tx, float ty, float tz){
        float translation_matrix[4][4];
        float temp_matrix[4][4];
        float aux, sum = 0;
        int i, j, k;

        // translation matrix definition
        translation_matrix[0][0] = 1;
        translation_matrix[0][1] = 0;
        translation_matrix[0][2] = 0;
        translation_matrix[0][3] = tx;

        translation_matrix[1][0] = 0;
        translation_matrix[1][1] = 1;
        translation_matrix[1][2] = 0;
        translation_matrix[1][3] = ty;

        translation_matrix[2][0] = 0;
        translation_matrix[2][1] = 0;
        translation_matrix[2][2] = 1;
        translation_matrix[2][3] = tz;

        translation_matrix[3][0] = 0;
        translation_matrix[3][1] = 0;
        translation_matrix[3][2] = 0;
        translation_matrix[3][3] = 1;

         // temporary_matrix = translation_matrix * transform_matrix
        for(i = 0; i < 4; i++){
            for(j = 0; j < 4; j++){
                for(k = 0; k < 4; k++){
                    aux = translation_matrix[i][k] * transform_matrix[k][j];
                    sum += aux;
                }
                temp_matrix[i][j] = sum;
                sum = 0;
            }
        }

        // transform_matrix = temporary_matrix
        for(i = 0; i < 4; i++){
            for(j = 0; j < 4; j++)
                transform_matrix[i][j] = temp_matrix[i][j];
        }
    }

    /* Multiplies the current transform matrix by a scale matrix */
    void concat_scale(float sx, float sy, float sz){
        float scale_matrix[4][4];
        float temp_matrix[4][4];
        float aux, sum = 0;
        int i, j, k;

        // scale matrix definition
        scale_matrix[0][0] = sx;
        scale_matrix[0][1] = 0;
        scale_matrix[0][2] = 0;
        scale_matrix[0][3] = 0;

        scale_matrix[1][0] = 0;
        scale_matrix[1][1] = sy;
        scale_matrix[1][2] = 0;
        scale_matrix[1][3] = 0;

        scale_matrix[2][0] = 0;
        scale_matrix[2][1] = 0;
        scale_matrix[2][2] = sz;
        scale_matrix[2][3] = 0;

        scale_matrix[3][0] = 0;
        scale_matrix[3][1] = 0;
        scale_matrix[3][2] = 0;
        scale_matrix[3][3] = 1;

         // temporary_matrix = scale_matrix * transform_matrix
        for(i = 0; i < 4; i++){
            for(j = 0; j < 4; j++){
                for(k = 0; k < 4; k++){
                    aux = scale_matrix[i][k] * transform_matrix[k][j];
                    sum += aux;
                }
                temp_matrix[i][j] = sum;
                sum = 0;
            }
        }

        // transform_matrix = temporary_matrix
        for(i = 0; i < 4; i++){
            for(j = 0; j < 4; j++)
                transform_matrix[i][j] = temp_matrix[i][j];
        }
    }

    /* Multiplies the current transform matrix by a rotation matrix (axis = {'x', 'y', 'z'}) */
    void concat_rotation(char axis, float angle_rad){
        float rotation_matrix[4][4];
        float temp_matrix[4][4];
        float aux, sum = 0;
        int i, j, k;

        // rotation matrix definition depends uppon the axis
        switch(axis){
            case('x'):
                rotation_matrix[0][0] = 1;
                rotation_matrix[0][1] = 0;
                rotation_matrix[0][2] = 0;
                rotation_matrix[0][3] = 0;

                rotation_matrix[1][0] = 0;
                rotation_matrix[1][1] = cos(angle_rad);
                rotation_matrix[1][2] = -sin(angle_rad);
                rotation_matrix[1][3] = 0;

                rotation_matrix[2][0] = 0;
                rotation_matrix[2][1] = sin(angle_rad);
                rotation_matrix[2][2] = cos(angle_rad);
                rotation_matrix[2][3] = 0;

                rotation_matrix[3][0] = 0;
                rotation_matrix[3][1] = 0;
                rotation_matrix[3][2] = 0;
                rotation_matrix[3][3] = 1;
            break;

            case('y'):
                rotation_matrix[0][0] = cos(angle_rad);
                rotation_matrix[0][1] = 0;
                rotation_matrix[0][2] = sin(angle_rad);
                rotation_matrix[0][3] = 0;

                rotation_matrix[1][0] = 0;
                rotation_matrix[1][1] = 1;
                rotation_matrix[1][2] = 0;
                rotation_matrix[1][3] = 0;

                rotation_matrix[2][0] = -sin(angle_rad);
                rotation_matrix[2][1] = 0;
                rotation_matrix[2][2] = cos(angle_rad);
                rotation_matrix[2][3] = 0;

                rotation_matrix[3][0] = 0;
                rotation_matrix[3][1] = 0;
                rotation_matrix[3][2] = 0;
                rotation_matrix[3][3] = 1;
            break;

            case('z'):
                rotation_matrix[0][0] = cos(angle_rad);
                rotation_matrix[0][1] = -sin(angle_rad);
                rotation_matrix[0][2] = 0;
                rotation_matrix[0][3] = 0;

                rotation_matrix[1][0] = sin(angle_rad);
                rotation_matrix[1][1] = cos(angle_rad);
                rotation_matrix[1][2] = 0;
                rotation_matrix[1][3] = 0;

                rotation_matrix[2][0] = 0;
                rotation_matrix[2][1] = 0;
                rotation_matrix[2][2] = 1;
                rotation_matrix[2][3] = 0;

                rotation_matrix[3][0] = 0;
                rotation_matrix[3][1] = 0;
                rotation_matrix[3][2] = 0;
                rotation_matrix[3][3] = 1;
            break;

            default:
                printf("ERROR: SPECIFY A VALID AXIS\n");
                exit(-1);
        }

         // temporary_matrix = rotation_matrix * transform_matrix
        for(i = 0; i < 4; i++){
            for(j = 0; j < 4; j++){
                for(k = 0; k < 4; k++){
                    aux = rotation_matrix[i][k] * transform_matrix[k][j];
                    sum += aux;
                }
                temp_matrix[i][j] = sum;
                sum = 0;
            }
        }

        // transform_matrix = temporary_matrix
        for(i = 0; i < 4; i++){
            for(j = 0; j < 4; j++)
                transform_matrix[i][j] = temp_matrix[i][j];
        }
    }

    /* Concatenates a View matrix based on the LookAt camera, transforming world coordinates to eye coordinates.
    The LookAt camera is defined by its position (eye), where it is looking at (target) and the vector defining the up direction (up_dir).
    The whole scene is then transladed by (target - eye) and rotated so the camera is looking to the -Z axis (Mv = Mr . Mt) */
    void concat_lookat_view(Point3Df eye, Point3Df target){
        // For simplicity, we assume the up_dir as (0,1,0), i.e., straight to +Y
        Point3Df up_dir, forward, left, up;
        float M_rotation[4][4];
        float forward_mod, tx, ty, tz;

        up_dir.x = 0;   up_dir.y = 1;   up_dir.z = 0;

        // Translation elements
        tx = target.x - eye.x;
        ty = target.y - eye.y;
        tz = target.z - eye.z;

        // forward = (eye - target)/||eye - target||
        forward.x = eye.x - target.x;
        forward.y = eye.y - target.y;
        forward.z = eye.z - target.z;
        forward_mod = sqrt(pow(forward.x,2)+pow(forward.y,2)+pow(forward.z,2));
        forward.x = forward.x / forward_mod;
        forward.y = forward.y / forward_mod;
        forward.z = forward.z / forward_mod;

        // left = up_dir x forward (left is normalized)
        left.x = up_dir.y*forward.z - up_dir.z*forward.y;
        left.y = up_dir.z*forward.x - up_dir.x*forward.z;
        left.z = up_dir.x*forward.y - up_dir.y*forward.x;

        // up = forward x left (up is normalized)
        up.x = forward.y*left.z - forward.z*left.y;
        up.y = forward.z*left.x - forward.x*left.z;
        up.z = forward.x*left.y - forward.y*left.x;

        M_rotation[0][0] = left.x;
        M_rotation[0][1] = left.y;
        M_rotation[0][2] = left.z;
        M_rotation[0][3] = 0;

        M_rotation[1][0] = up.x;
        M_rotation[1][1] = up.y;
        M_rotation[1][2] = up.z;
        M_rotation[1][3] = 0;

        M_rotation[2][0] = forward.x;
        M_rotation[2][1] = forward.y;
        M_rotation[2][2] = forward.z;
        M_rotation[2][3] = 0;

        M_rotation[3][0] = 0;
        M_rotation[3][1] = 0;
        M_rotation[3][2] = 0;
        M_rotation[3][3] = 1;

        concat_translation(tx, ty, tz);
        concat_arbitrary(M_rotation);
    }

    /* Concatenates a View matrix based on the LookAt camera, transforming world coordinates to eye coordinates.
    The lookaat camera is defined by its position (eye), where it is looking at (target) and the vector defining the up direction (up_dir).
    The whole scene is then transladed by (target - eye) and rotated so the camera is looking to the -Z axis (Mv = Mr . Mt) */
    void concat_lookat_view(float Ex, float Ey, float Ez, float Tx, float Ty, float Tz){
        // For simplicity, we assume the up_dir as (0,1,0), i.e., straight to +Y
        Point3Df eye, target;
        Point3Df up_dir, forward, left, up;
        float M_rotation[4][4];
        float forward_mod, tx, ty, tz;

        eye.x = Ex;     eye.y = Ey;     eye.z = Ez;
        target.x = Tx;  target.y = Ty;  target.z = Tz;
        up_dir.x = 0;   up_dir.y = 1;   up_dir.z = 0;

        // Translation elements
        tx = target.x - eye.x;
        ty = target.y - eye.y;
        tz = target.z - eye.z;

        // forward = (eye - target)/||eye - target||
        forward.x = eye.x - target.x;
        forward.y = eye.y - target.y;
        forward.z = eye.z - target.z;
        forward_mod = sqrt(pow(forward.x,2)+pow(forward.y,2)+pow(forward.z,2));
        forward.x = forward.x / forward_mod;
        forward.y = forward.y / forward_mod;
        forward.z = forward.z / forward_mod;

        // left = up_dir x forward (left is normalized)
        left.x = up_dir.y*forward.z - up_dir.z*forward.y;
        left.y = up_dir.z*forward.x - up_dir.x*forward.z;
        left.z = up_dir.x*forward.y - up_dir.y*forward.x;

        // up = forward x left (up is normalized)
        up.x = forward.y*left.z - forward.z*left.y;
        up.y = forward.z*left.x - forward.x*left.z;
        up.z = forward.x*left.y - forward.y*left.x;

        M_rotation[0][0] = left.x;
        M_rotation[0][1] = left.y;
        M_rotation[0][2] = left.z;
        M_rotation[0][3] = 0;

        M_rotation[1][0] = up.x;
        M_rotation[1][1] = up.y;
        M_rotation[1][2] = up.z;
        M_rotation[1][3] = 0;

        M_rotation[2][0] = forward.x;
        M_rotation[2][1] = forward.y;
        M_rotation[2][2] = forward.z;
        M_rotation[2][3] = 0;

        M_rotation[3][0] = 0;
        M_rotation[3][1] = 0;
        M_rotation[3][2] = 0;
        M_rotation[3][3] = 1;

        concat_translation(tx, ty, tz);
        concat_arbitrary(M_rotation);
    }

    /* Multiplies the current transform matrix by an orthographic projection matrix, transforming from eye coordinates to clip coordinates. */
    void concat_ortho_proj(float left, float right, float bottom, float top){
        float c_near = -10;
        float c_far = 10;

        concat_scale(2/(right-left), 2/(top - bottom), -2/(c_far-c_near));
        concat_translation(-(right+left)/(right-left), -(top+bottom)/(top-bottom), -(c_far+c_near)/(c_far-c_near));
    }

    /* Multiplies the current transform matrix by a perspective projection matrix, transforming from eye coordinates to clip coordinates. */
    void concat_persp_proj(float aspect, float zNear, float zFar){
        float persp_matrix[4][4];
        float temp_matrix[4][4];
        float f, fovy = PI/3;
        float aux, sum = 0;
        int i, j, k;

        f = 1/tan(fovy/2);

        // perspective projection matrix definition
        persp_matrix[0][0] = f/aspect;
        persp_matrix[0][1] = 0;
        persp_matrix[0][2] = 0;
        persp_matrix[0][3] = 0;

        persp_matrix[1][0] = 0;
        persp_matrix[1][1] = f;
        persp_matrix[1][2] = 0;
        persp_matrix[1][3] = 0;

        persp_matrix[2][0] = 0;
        persp_matrix[2][1] = 0;
        persp_matrix[2][2] = (zFar + zNear)/(zNear - zFar);
        persp_matrix[2][3] = (2*zFar*zNear)/(zNear - zFar);

        persp_matrix[3][0] = 0;
        persp_matrix[3][1] = 0;
        persp_matrix[3][2] = -1;
        persp_matrix[3][3] = 0;

         // temporary_matrix = persp_matrix * transform_matrix
        for(i = 0; i < 4; i++){
            for(j = 0; j < 4; j++){
                for(k = 0; k < 4; k++){
                    aux = persp_matrix[i][k] * transform_matrix[k][j];
                    sum += aux;
                }
                temp_matrix[i][j] = sum;
                sum = 0;
            }
        }

        // transform_matrix = temporary_matrix
        for(i = 0; i < 4; i++){
            for(j = 0; j < 4; j++)
                transform_matrix[i][j] = temp_matrix[i][j];
        }
    }

    /* Concatenates a matrix that transform viewport coordinates to screen coordinates */
    void concat_viewport_to_screen(int window_width, int window_height){
        float vts_matrix[4][4];
        float temp_matrix[4][4];
        float aux, sum = 0;
        int i, j, k;

        // viewport to screen coord matrix definition
        vts_matrix[0][0] = window_width;
        vts_matrix[0][1] = 0;
        vts_matrix[0][2] = 0;
        vts_matrix[0][3] = window_width/2;

        vts_matrix[1][0] = 0;
        vts_matrix[1][1] = window_height;
        vts_matrix[1][2] = 0;
        vts_matrix[1][3] = window_height/2;

        vts_matrix[2][0] = 0;
        vts_matrix[2][1] = 0;
        vts_matrix[2][2] = 1;
        vts_matrix[2][3] = 0;

        vts_matrix[3][0] = 0;
        vts_matrix[3][1] = 0;
        vts_matrix[3][2] = 0;
        vts_matrix[3][3] = 1;

         // temporary_matrix = vts_matrix * transform_matrix
        for(i = 0; i < 4; i++){
            for(j = 0; j < 4; j++){
                for(k = 0; k < 4; k++){
                    aux = vts_matrix[i][k] * transform_matrix[k][j];
                    sum += aux;
                }
                temp_matrix[i][j] = sum;
                sum = 0;
            }
        }

        // transform_matrix = temporary_matrix
        for(i = 0; i < 4; i++){
            for(j = 0; j < 4; j++)
                transform_matrix[i][j] = temp_matrix[i][j];
        }
    }

    /* Multiplies the current transform matrix by an arbitrary 4x4 matrix */
    void concat_arbitrary(float arbitrary_matrix[4][4]){
        int i, j, k;
        float aux, sum = 0;
        float temp_matrix[4][4];

        // temporary_matrix = arbitrary_matrix * transform_matrix
        for(i = 0; i < 4; i++){
            for(j = 0; j < 4; j++){
                for(k = 0; k < 4; k++){
                    aux = arbitrary_matrix[i][k] * transform_matrix[k][j];
                    sum += aux;
                }
                temp_matrix[i][j] = sum;
                sum = 0;
            }
        }

        // transform_matrix = temporary_matrix
        for(i = 0; i < 4; i++){
            for(j = 0; j < 4; j++)
                transform_matrix[i][j] = temp_matrix[i][j];
        }

    }

    /* Defines a dot product between the transform matrix and an arbitrary 3D point.
    In this method is taken the divider step, transforming the clip coordinates to viewport coordinates*/
    Point3Df operator *(Point3Df point){

        float aux, sum = 0;
        int i, j;
        float point_vec[4], resulting_point_vec[4];
        Point3Df resulting_point;

        // converts point to homogeneous coordinates
        point_vec[0] = point.x;
        point_vec[1] = point.y;
        point_vec[2] = point.z;
        point_vec[3] = 1;

        // apply transform
        for(i = 0; i < 4; i++){
            for(j = 0; j < 4; j++){
                aux = transform_matrix[i][j] * point_vec[j];
                sum += aux;
            }
            resulting_point_vec[i] = sum;
            sum = 0;
        }

        resulting_point.x = resulting_point_vec[0]/resulting_point_vec[3];
        resulting_point.y = resulting_point_vec[1]/resulting_point_vec[3];
        resulting_point.z = resulting_point_vec[2]/resulting_point_vec[3];

        return resulting_point;
    }

    /* The object's transform_matrix becomes the product BxA */
    void join_transforms(Transform_3D A, Transform_3D B){
        float mat_A[4][4], mat_B[4][4];
        int i, j;

        // gets transforms matrices
        for(i = 0; i < 4; i++)
            for(j = 0; j < 4; j++){
                mat_A[i][j] = A.get_member(i,j);
                mat_B[i][j] = B.get_member(i,j);
            }

        concat_arbitrary(mat_A);
        concat_arbitrary(mat_B);

    }

    /* Returns a specific member of the transform matrix */
    float get_member(int line_index, int col_index){
        return transform_matrix[line_index][col_index];
    }

    /* Prints the current transform matrix */
    void print_matrix(){
        int i, j;

        printf("[CURRENT TRANSFORMATION MATRIX]\n");
        for(i = 0; i < 4; i++){
            for(j = 0; j < 4; j++){
                printf("%g  ", transform_matrix[i][j]);
            }
            printf("\n");
        }
    }

};

#endif
