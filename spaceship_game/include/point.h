/* Arquivo que define estruturas para pontos 2D e 3D (float e double) */

#ifndef POINT_H
#define POINT_H

typedef struct point2d{
    float x, y;
} Point2D;

typedef struct point2xz{
    float x, z;
} Point2xz;

typedef struct point3df{
    float x, y, z;
} Point3Df;

typedef struct point3dd{
    double x, y, z;
} Point3Dd;

#endif
