#ifndef SPACE_H
#define SPACE_H

#include <GL/glut.h>
#include <GL/freeglut_ext.h>
#include <math.h>
#include <shapes3D_gl.hpp>
#include <frames.h>
#include <strings.h>
#include <stdlib.h>

#ifndef PI
#define PI      3.14159265358979323846
#endif

#define STATE_ON 1
#define STATE_OFF 0

#define MAX_X 100
#define MAX_Z 230
#define MIN_X -100
#define MIN_Z -200

#define COIN_RADIUS 8.0
#define COMET_RADIUS 16.0
#define SHIP_INFLUENCE 17.0
#define OBJ_SCR_TIME 10
#define SHIP_SCR_TIME 5
#define OBJ_FRM_DIST 1000

using std::vector;
/*
void text(int x, int y, const char *t)
{
    int tam = (int)strlen(t);
    int c;

    for(c=0; c<tam; c++)
    {
      glRasterPos2i(x + c*10, y);
      glutBitmapCharacter(GLUT_BITMAP_8_BY_13, t[c]);
    }
}*/

class Space_ship
{
//
private:
    Point2xz position;
    Cylinder *back_cy;
    Cuboid *main_cb, *lwing_cb, *rwing_cb;
    float x_increment;
    Frames frm;

public:
    Space_ship()
    {
        main_cb = new Cuboid(-5,0,30.0,10.0,10.0);
        lwing_cb = new Cuboid(-7.5,0,20,2.5,2.5);
        rwing_cb = new Cuboid(5,0,20,2.5,2.5);
        back_cy = new Cylinder(-4,0,4,9);
        position.x = 0; position.z = 160;
        x_increment = 0;
    }

    void calc_x_increment(float seconds){
        // Argument: time that ship will take to cross the screen
        float FPS;

        FPS = frm.getFrames();
        x_increment = (MAX_X-MIN_X)*(1/FPS)*(1/seconds);
        //printf("inc: %f\n",x_increment);

    }

    void render(){
        calc_x_increment(SHIP_SCR_TIME);
        glPushMatrix();

        glTranslatef(position.x, 0.0, position.z + 13);
        glRotatef(90, 0.0, 0.0, 1.0);

        glPushMatrix();
        glRotatef(-90, 1.0, 0.0, 0.0);
        glColor3f(0.9, 0.9, 0.9);
        main_cb->render();
        glColor3f(0.7, 0.7, 0.7);
        lwing_cb->render();
        rwing_cb->render();
        glPopMatrix();

        glRotatef(90, 0.0, 1.0, 0.0);
        glColor3f(0.7, 0.7, 0.7);
        back_cy->render();

        glPopMatrix();
    }


    void go_up(){
        if(position.x > MIN_X)
            position.x -= x_increment;

    }

    void go_down(){
        if(position.x < MAX_X)
            position.x += x_increment;

    }

    Point2xz get_position(){
        return position;
    }

    void set_xposition(int posx){
        position.x = posx;
    }

};

class Comet
{
private:
    Point2xz position;

public:
    Comet()
    {
        position.x = 0.0;
        position.z = 0.0;
    }

    void render(){
        glPushMatrix();
        glTranslatef(position.x, 0, position.z);
        glColor3f(0.0, 0.0, 1.0);
        glutSolidSphere(COMET_RADIUS, 20, 20);
        glPopMatrix();
    }

    void set_position(float px, float pz){
        position.x = px;
        position.z = pz;
    }

    Point2xz get_position(){
        return position;
    }

};

class Coin
{
private:
    float angle_y;
    Point2xz position;
    //float angle_increment;
    Cylinder *coin_cylinder;
    Frames frm;

public:
    Coin()
    {
        coin_cylinder = new Cylinder(-1,0,2.0,COIN_RADIUS);
        angle_y = 0;
    }

    void inc_angle_y(float RPM){
        float FPS, angle_increment;

        FPS = frm.getFrames();
        angle_increment = (2*PI*RPM)/(60*FPS);

        if(angle_y < 2*PI)
            angle_y += angle_increment;
        else
            angle_y = 0;
    }

    void render(){
        float dg_angle_y;
        inc_angle_y(30);

        dg_angle_y = angle_y*180/PI;

        glPushMatrix();
        glTranslatef(position.x, 0, position.z);
        glRotatef(30, 0.0, 0.0, 1.0);
        glRotatef(dg_angle_y, 0.0, 1.0, 0.0);
        glColor3f(1.0,1.0,0.2);                                     // Yellow
        coin_cylinder->render();
        glPopMatrix();
    }

    void set_position(float px, float pz){
        position.x = px;
        position.z = pz;
    }

    Point2xz get_position(){
        return position;
    }
};

class Background_stars
{
private:
    short int state;
    unsigned int quantity;
    Point2D *stars_position;

    //int win_height, win_width;

public:

    Background_stars(int window_width, int window_height, int quant){
        unsigned int i;

        state = STATE_ON;
        quantity = quant;

        stars_position = (Point2D*) malloc(quantity*sizeof(Point2D));
        for(i = 0; i < quantity; i++){
            stars_position[i].x = rand() % window_width;
            stars_position[i].y = rand() % window_height;
        }
    }


    void render(){
        unsigned int i;

        if(state == STATE_ON){
        glPointSize(1);
        glColor3f(1.0, 1.0, 1.0);
        glBegin(GL_POINTS);
        for(i = 0; i < quantity; i++)
            glVertex2d(stars_position[i].x, stars_position[i].y);
        glEnd();
        }
    }


    void set_state(int st){
        if(st == STATE_ON or st == STATE_OFF){
            state = st;
        }
        else
            fprintf(stderr, "ERROR, please enter a valid state (1 or 0)\n");
    }

};


class Brain_lightyear
{
// Class defining the game itself
private:

    vector<Coin> coins;
    vector<Comet> comets;
    Space_ship ship;
    Background_stars *stars;

    int score;
    float z_increment;
    long int frame_counter;
    Frames frm;
    bool paused;

    int win_width, win_height;
    Point3Dd camera_eye;                        // the camera eye according to the lookat camera setting
    Point3Dd camera_target;                     // the camera target according to the lookat camera

    char attention_lvl;                         // Attention level
    char old_attention_lvl;                     // Old attention level
    char attention_derivative;                   // Derivative of the attention level

public:
    Brain_lightyear(int window_width, int window_height)
    {
        score = 0;

        win_width = window_width;
        win_height = window_height;
        stars = new Background_stars(win_width, win_height, 100);
        camera_eye = {0,200,0};
        camera_target = {0,0,0};

        frame_counter = 0;
        z_increment = 0;

        srand(time(NULL));
        paused = false;

        attention_lvl = 50;
        old_attention_lvl = 50;
        attention_derivative = 0;
    }

    void generate_object(){
        int object;
        Point2xz position;

        position.z = MIN_Z;
        position.x = rand()%(MAX_X - MIN_X) - MAX_X;
        object = rand()%2;                                      // COIN -> object = 1; COMET -> object = 0;
        if(object == 1){                                        // Coin
            Coin new_coin;

            new_coin.set_position(position.x, position.z);
            coins.push_back(new_coin);
        }
        else{                                                   // Comet
            Comet new_comet;

            new_comet.set_position(position.x, position.z);
            comets.push_back(new_comet);
        }
    }

    void calc_z_increment(float seconds){
        // Argument: time that objects will take to cross the screen
        float FPS;

        FPS = frm.getFrames();
        z_increment = (MAX_Z-MIN_Z)*(1/FPS)*(1/seconds);

    }

    void move_objects(){
        Point2xz current_pos;
        unsigned int i;
        calc_z_increment(OBJ_SCR_TIME);
        for(i=0; i<coins.size(); i++){

            current_pos = coins[i].get_position();
            current_pos.z += z_increment;
            coins[i].set_position(current_pos.x, current_pos.z);
        }

        for(i=0; i<comets.size(); i++){
            current_pos = comets[i].get_position();
            current_pos.z += z_increment;
            comets[i].set_position(current_pos.x, current_pos.z);
        }
    }

    float calc_xz_distance(Point2xz obj1, Point2xz obj2){
        float distance;

        distance = sqrt( pow(obj1.x - obj2.x, 2) + pow(obj1.z - obj2.z, 2) );
        return distance;
    }

    void restart(){
        coins.clear();
        comets.clear();
        score = 0;
        ship.set_xposition(0);
    }

    void detect_colision(){
        unsigned int i;
        float dist;

        for(i=0; i < coins.size(); i++){
            dist = calc_xz_distance(coins[i].get_position(), ship.get_position()) - COIN_RADIUS - SHIP_INFLUENCE;
            if(dist <= 0){
                coins.erase(coins.begin()+i);
                score++;
            }
        }

        for(i=0; i < comets.size(); i++){
            dist = calc_xz_distance(comets[i].get_position(), ship.get_position()) - COMET_RADIUS - SHIP_INFLUENCE;
            if(dist <= 0)
                restart();

        }

    }

    void detect_endpoint(){
        unsigned int i;
        Point2xz position;

        for(i=0; i < coins.size(); i++){
            position = coins[i].get_position();
            if(position.z >= MAX_Z)
                coins.erase(coins.begin()+i);

        }

        for(i=0; i < comets.size(); i++){
            position = comets[i].get_position();
            if(position.z >= MAX_Z)
                comets.erase(comets.begin()+i);
        }
    }

    /////////////////////////////////////////
    // MIND METHODS
    void update_attention(){
        FILE *fp;
        char attention_temp;

        fp = fopen("holder.bin","rb");
        if(!fp){
            printf("unable to open\n");
        }
        else{
            fread(&attention_temp,sizeof(attention_temp),1,fp);
            fclose(fp);
            old_attention_lvl = attention_lvl;
            attention_lvl = attention_temp;
        }
    }

    void update_derivative(){
        if (attention_lvl != old_attention_lvl)
            attention_derivative = attention_lvl - old_attention_lvl;
    }

    void move_ship(){
        update_attention();
        update_derivative();

        if (attention_derivative >= 5)
            ship.go_up();
        else if (attention_derivative <= -5)
            ship.go_down();

        /*if(attention_lvl < 33)
            ship.go_down();
        else if (attention_lvl > 66)
            ship.go_up();*/
    }
    /////////////////////////////////////////

    void run(){

        if(paused){

        }
        else{
            frame_counter++;
            // PARAMETERIZE
            if(frame_counter > OBJ_FRM_DIST){
                generate_object();
                frame_counter = 0;
            }

            move_ship();
            move_objects();
            detect_colision();
            detect_endpoint();
        }

        render();
    }

    void render(){
        unsigned int i;
        char score_txt[20] = "Score: ";
        char attention_txt[20] = "Attention: ";
        char derivative_txt[20] = "Derivative: ";

        glClearColor (0.0,0.0,0.0,1.0);
        glClear (GL_COLOR_BUFFER_BIT);

        // 2D
        // Projection
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, win_width, 0, win_height, 1, -1);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glDisable(GL_DEPTH_TEST);
        glDisable(GL_LIGHTING);

        stars->render();


        // 3D
        // Projection
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_LIGHTING);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity ();
        gluPerspective(60, (GLfloat)win_width/(GLfloat)win_height, 1.0, 10000.0);
        // Camera
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        gluLookAt((GLdouble)camera_eye.x,    (GLdouble)camera_eye.y,    (GLdouble)camera_eye.z,                  // eye
                  (GLdouble)camera_target.x, (GLdouble)camera_target.y, (GLdouble)camera_target.z,               // target
                  -1, 0, 0);                                                                                     // up


        for(i=0; i < coins.size(); i++)
            coins[i].render();

        for(i=0; i < comets.size(); i++)
            comets[i].render();

        ship.render();

        // 2D
        // Projection
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, win_width, 0, win_height, 1, -1);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glDisable(GL_DEPTH_TEST);
        glDisable(GL_LIGHTING);

        // Text
        glColor3f(1.0, 1.0, 1.0);
        strcat(score_txt, patch::to_string(score).c_str());
        text(win_width-155, win_height-20, score_txt);
        strcat(attention_txt, patch::to_string((int)attention_lvl).c_str());
        text(win_width-155, win_height-50, attention_txt);
        strcat(derivative_txt, patch::to_string((int)attention_derivative).c_str());
        text(win_width-155, win_height-80, derivative_txt);

        if(paused){
            char paused_str[10] = "PAUSED";
            //printf("%s\n",paused_str);

            glColor3f(1.0, 1.0, 1.0);
            text(win_width/2, win_height-100, paused_str);
        }
    }

    void pause(){
        paused = true;
    }

    void unpause(){
        paused = false;
    }

    void check_kb(int key){
        switch(key){
                /*case 119:                               // w
                    if(!paused)
                        ship.go_up();
                    break;*/

                case 112:
                    if(paused)
                        unpause();
                    else
                        pause();
                    break;

                /*case 115:                               // s
                    if(!paused)
                        ship.go_down();
                    break;*/

                default:
                    ;
        }
    }


};
#endif //SPACE_H
