/*
Arquivo contendo as classes destinadas a componentes de interface gráfica.
Herdam da classe Component as classes Button, TrackBar, CheckBox, CheckBoxGroup e PlotArea.

São utilizadas funções retiradas da API Canvas (OpenGL) para representação gráfica.

Victor O. Costa
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <vector>
#include <string>
#include <sstream>
#include <to_str.hpp>

#define BUTTON_HEIGHT 25
#define BUTTON_WIDTH 100
#define CHECKBOX_SIZE 20
#define PLOT_AREA_HEIGHT 480
#define PLOT_AREA_WIDTH 1180
#define P_A_SLACK 30                                                                                                    // distance between the external rectangle and the actual plotting area
#define BAR_HEIGHT 6
#define BAR_WIDTH 16
#define PI_2 6.28318530717958

using std::vector;
using std::string;

/////////////// CANVAS DEPENDENCIES /////////////////////
// Following functions author: Cesar Tadeu Pozzer
void line( int x1, int y1, int x2, int y2 )
{
   glBegin(GL_LINES);
      glVertex2d(x1, y1);
      glVertex2d(x2, y2);
   glEnd();
}

void rectFill( int x1, int y1, int x2, int y2 )
{
   glBegin(GL_QUADS);
      glVertex2d(x1, y1);
      glVertex2d(x1, y2);
      glVertex2d(x2, y2);
      glVertex2d(x2, y1);
   glEnd();
}

void text(int x, int y, const char *t)
{
    int tam = (int)strlen(t);
    int c;

    for(c=0; c<tam; c++)
    {
      glRasterPos2i(x + c*10, y);
      glutBitmapCharacter(GLUT_BITMAP_8_BY_13, t[c]);
    }
}

void circle( int x, int y, int raio, int div )
{
   float ang, x1, y1;
   float inc = PI_2/div;
   glBegin(GL_LINE_LOOP);
      for(ang=0; ang<6.27; ang+=inc) //nao vai ateh PI_2 pois o ultimo ponto eh fechado automaticamente com o primeiro, pois tem o tipo LINE_LOOP
      {
         x1 = (cos(ang)*raio);
         y1 = (sin(ang)*raio);
         glVertex2d(x1+x, y1+y);
      }
   glEnd();
}

void circleFill( int x, int y, int raio, int div )
{
   float ang, x1, y1;
   float inc = PI_2/div;
   glBegin(GL_POLYGON);
      for(ang=0; ang<6.27; ang+=inc)
      {
         x1 = (cos(ang)*raio);
         y1 = (sin(ang)*raio);
         glVertex2d(x1+x, y1+y);
      }
   glEnd();
}

void color(float r, float g, float b)
{
   glColor3d(r, g, b  );
}
/////////////////////////////////////////////////////////

class Component{
/* Parent class for GUI components */

protected:
    int posx, posy, height, width;
    bool enabled, being_clicked;

    // converting num to string (MinGW bug fix)
    string to_string(float num){
        std::ostringstream ss;

        ss << num;
    return ss.str();
    }


public:


    Component(int x, int y, int he, int wi, bool en){
        posx = x;
        posy = y;
        height = he;
        width = wi;
        enabled = en;
        being_clicked = false;
    }

    bool click_in_area(int x, int y){
    /* Method to check if the click was in the component's area*/

        int lim_x, lim_y;

        lim_x = posx + width;
        lim_y = posy + height;
        return ((x>=posx)&&(x<=lim_x)&&(y>=posy)&&(y<=lim_y));                                                          // click location logic
    }

    void enable(){
        enabled = true;
    }

    void disable(){
        enabled = false;
    }

    virtual void render() {}                                                                                            // all children classes must implement a rendering method
    virtual void mouse_check(int button, int state, int x, int y) {}                                                    // all children classes must implement a method to handle mouse actions
};

class TrackBar : public Component{
private:
    float bar_position;                                                                                                 // range = [0, 1]
    float min_val, max_val;
    bool moving_bar;
    char label[15];
    int label_shift;
public:
    TrackBar(int x, int y, int h, float min_v, float max_v, int lbl_shift,const char *lbl) : Component(x, y, h, 5, false){
        strcpy(label, lbl);
        bar_position = 0.5;
        moving_bar = false;
        min_val = min_v;
        max_val = max_v;
        label_shift = lbl_shift;
    }

    void render(){
    /* Renders the component */
        float bar_y_center;
        char aux_label[15];

        color(0.2, 0.2, 0.2);
        rectFill(posx - width/2, posy, posx + width/2, posy + height);                                                  // draw scale

        bar_y_center = posy + bar_position*height;
        color(0.8, 0.8, 0.8);
        rectFill(posx - BAR_WIDTH/2, bar_y_center - BAR_HEIGHT/2, posx + BAR_WIDTH/2, bar_y_center + BAR_HEIGHT/2);    // draw bar

        strcpy(aux_label, label);
        strcat(aux_label,patch::to_string(get_value()).c_str());
        color(0,0,0);
        text(posx-label_shift, posy+height+10, aux_label);
    }

    void mouse_check(int button, int state, int x, int y)
    /* Checks mouse actions */
    {
        if(enabled){
             if(button == GLUT_LEFT_BUTTON){
                if((state == GLUT_DOWN) && (click_in_bar(x, y)))
                    moving_bar = true;

                else if(state == GLUT_UP)
                    moving_bar = false;
            }

            if(moving_bar){
                if(y < posy)
                    bar_position = 0;
                else if(y > (posy + height))
                    bar_position = 1;
                else{
                    bar_position = ((float)(y - posy))/height;
                }
            }
        }

    }

    bool click_in_bar(int x, int y){
    /* Checks if the click is in the bar's area */
        int left_edge, bottom_edge, right_edge, upper_edge;

        left_edge = posx - BAR_WIDTH/2 - 2;
        bottom_edge = posy + bar_position*height - BAR_HEIGHT/2 - 2;
        right_edge = posx + BAR_WIDTH/2 + 2;
        upper_edge = posy + bar_position*height + BAR_HEIGHT/2 + 2;

        return ((x > left_edge && x < right_edge) && (y > bottom_edge && y < upper_edge));
    }

    float get_value(){
    /* Get the current value represented by the bar position */
        float current_val;

        current_val = min_val + (max_val - min_val)*bar_position;
        return current_val;
    }

    void set_range(float min, float max){
        min_val = min;
        max_val = max;
    }
};

class Button : public Component{
/* Button component, that reacts to a left button click/release by calling a click handler method (callback) */

private:
    string label;
    int label_distance;
    void (*click_handler)();                                                                                // Button click callback
public:
    Button(int x, int y, const char * lbl, int lbl_dst, void (*click_callback)()) : Component(x, y, BUTTON_HEIGHT, BUTTON_WIDTH, false){
        label = lbl;                                                                                        // label to appear in the button
        label_distance = lbl_dst;                                                                           // label positioning
        click_handler = click_callback;
    }

    void render(){
        int lim_x, lim_y;
        lim_x = posx + width;
        lim_y = posy + height;

        if(being_clicked)                                                                                   // color logic according to clicks
            color(0.75,0.75,0.75);
        else
            color(0.9,0.9,0.9);

        rectFill(posx, posy, lim_x, lim_y);

        if(enabled)                                                                                         // color logic according to the button state
            color(0,0,0);
        else
            color(0.4,0.4,0.4);

        line(posx, posy, lim_x, posy);
        line(lim_x, posy, lim_x, lim_y);
        line(lim_x, lim_y, posx, lim_y);
        line(posx, lim_y +1, posx, posy);
        text(posx + label_distance, posy + 10, label.c_str());
    }

    void mouse_check(int button, int state, int x, int y){
        if(enabled){
            if(button == GLUT_LEFT_BUTTON){                                                                 // if left mouse button
                switch(state){
                    case GLUT_DOWN:                                                                         // if mouse click
                        if(click_in_area(x, y)){
                            being_clicked = true;
                        }
                    break;

                    case GLUT_UP:                                                                           // if mouse release
                        if(click_in_area(x,y) && being_clicked){
                            click_handler();                                                                // button action callback
                        }
                        being_clicked = false;
                    break;
                }
            }
        }

    }

};


class CheckBox : public Component{
/* CheckBox component that reacts to a click by changing its checked status */

private:
    bool checked;
    string label;
    char shape;                                                                                             // 'c' for round, 's' for square

public:
    CheckBox(int x, int y, const char * lbl, const char shp) : Component(x, y, CHECKBOX_SIZE, CHECKBOX_SIZE, false){
        label = lbl;
        checked = false;
        shape = shp;
    }

    void render(){
        int lim_x, lim_y;
        float center_x, center_y;
        lim_x = posx + width;
        lim_y = posy + height;
        center_x = posx + (float)width/2;
        center_y = posy + (float)height/2;

        if(being_clicked)
            color(0.8, 0.8, 0.8);
        else
            color(0.9,0.9,0.9);

        if(shape == 's')
            rectFill(posx, posy, lim_x, lim_y);
        else
            circleFill((int)center_x, (int)center_y, width/2, 10);

        if(enabled)
            color(0, 0, 0);
        else
            color(0.4, 0.4, 0.4);

        if(shape == 's'){
            line(posx, posy, lim_x, posy);
            line(lim_x, posy, lim_x, lim_y);
            line(lim_x, lim_y, posx, lim_y);
            line(posx, lim_y +1, posx, posy);
        }
        else{
            circle(center_x, center_y, width/2, 10);
        }

        color(0,0,0);
        text(posx + width + 5, posy + 7, label.c_str());

        if(checked){
            color(0, 1, 0);
            if(shape == 's'){
                rectFill((int)(posx + width/4), (int)(posy + height/4), (int)(lim_x - width/4), (int)(lim_y - height/4));
            }
            else{
                float x_middle, y_middle;
                x_middle = posx + (float)width/2;
                y_middle = posy + (float)height/2;

                circleFill((int)x_middle, (int)y_middle, 5, 10);
            }
        }

    }

    void mouse_check(int button, int state, int x, int y){
        if(enabled){
            if(button == GLUT_LEFT_BUTTON){                                                                 // if left mouse button
                switch(state){
                    case GLUT_DOWN:                                                                         // if mouse click
                        if(click_in_area(x, y)){
                            being_clicked = true;
                        }
                    break;

                    case GLUT_UP:                                                                           // if mouse release
                        if(click_in_area(x,y) && being_clicked){
                            checked = !checked;                                                             // check/uncheck the checkbox
                        }
                        being_clicked = false;
                    break;
                }
            }
        }
    }

    void disable(){
        Component::disable();
        checked = false;
    }

    bool get_checked(){
        return checked;
    }

    void set_checked(bool status){
        checked = status;
    }

    bool get_being_clicked(){
        return being_clicked;
    }

    void set_being_clicked(bool status){
        being_clicked = status;
    }

    const char * get_label(){
        return label.c_str();
    }
};


class CheckBoxGroup : public Component{
/* Group of checkboxes, where only one checkbox can be checked in any given time */

private:


public:
    vector<CheckBox> members;                                                                               // vector containing checkboxes
    int checked_member;                                                                                     // identifies which member of the group is checked

    CheckBoxGroup(int x, int y) : Component(x, y, 0, 0, false){
        checked_member = -1;
    }

    void check_member(int member_index){
    /* Method to set a member as checked and uncheck all the others */

        int i;
        checked_member = member_index;

        for(i = 0; i < (int)members.size(); i++){
            if(i == member_index)
                members[i].set_checked(true);
            else
                members[i].set_checked(false);
        }

    }

    void render(){
        unsigned int i;
        for(i = 0; i < members.size(); i++){
            members[i].render();
        }
    }

    void enable(){
        unsigned int i;
        Component::enable();
        for(i = 0; i < members.size(); i++){
            members[i].enable();
        }
    }

    void disable(){
        unsigned int i;
        for(i = 0; i < members.size(); i++){
            members[i].disable();
        }
    }

    void mouse_check(int button, int state, int x, int y){
        unsigned int i;

        if(enabled){
            if(button == GLUT_LEFT_BUTTON){                                                                 // if left mouse button
                switch(state){
                    case GLUT_DOWN:                                                                         // if mouse click
                        for(i = 0; i < members.size(); i++){
                            if(members[i].click_in_area(x, y)){
                                members[i].set_being_clicked(true);
                            }
                        }
                    break;

                    case GLUT_UP:                                                                           // if mouse release
                        for(i = 0; i < members.size(); i++){
                            if(members[i].click_in_area(x,y) && members[i].get_being_clicked()){
                                check_member(i);
                                members[i].set_being_clicked(false);
                            }
                        }
                    break;
                }
            }
        }
    }

    void add(int relative_x, int relative_y, const char * lbl){
    /* Method to insert a new checkbox to the group */

        int x, y;
        x = posx + relative_x;
        y = posy + relative_y;

        CheckBox new_member(x, y, lbl, 'c');
        members.push_back(new_member);
    }

    const char * get_checked_member(){
    /* Method that returns the label of the checked member */

        if(checked_member != -1)
            return members[checked_member].get_label();
        else
            return "NONE";
    }


};


class PlotArea : public Component{
private:
    unsigned int max_abs_value;                                                                             // maximum absolute value among all values to be plotted
    unsigned int max_lenght;                                                                                // maximum lenght among the sample groups to be plotted
    unsigned short min_distance;                                                                            // minimum distance between plotting points
    vector<signed short> samples;
    vector<signed short> reconstructed_samples;

public:
    PlotArea(int x, int y, unsigned short min_dist) : Component(x, y, PLOT_AREA_HEIGHT, PLOT_AREA_WIDTH, true){
        min_distance = min_dist;
        max_abs_value = 0;
        max_lenght = 0;
    }

    void add_samples(vector<signed short> samp){
        /* Incorporates the samples vector */
        unsigned int i, abs_val;

        samples = samp;

        if(samples.size() > max_lenght)                                                                     // updates the maximum lenght
            max_lenght = samples.size();

        for(i = 0; i < samples.size(); i++){                                                                // updates the maximum absolute value
            abs_val = abs(samples.at(i));
            if(abs_val > max_abs_value)
                max_abs_value = abs_val;
        }

    }

    void add_rsamples(vector<signed short> rsamp){
        /* Incorporates the reconstructed samples */
        unsigned int i, abs_val;

        reconstructed_samples = rsamp;

        if(reconstructed_samples.size() > max_lenght)                                                        // updates the maximum lenght
            max_lenght = reconstructed_samples.size();

        for(i = 0; i < reconstructed_samples.size(); i++){                                                   // updates the maximum absolute value
            abs_val = abs(reconstructed_samples.at(i));
            if(abs_val > max_abs_value)
                max_abs_value = abs_val;
        }

    }

    void render_structure(){
        unsigned int i;
        int lim_x, lim_y, y_ref, x_ref;
        float values_dist, samples_dist;

        lim_x = posx + width;
        lim_y = posy + height;
        y_ref = (posy + lim_y)/2;                                                                           // y axis reference for the point (0,0)
        x_ref = posx + P_A_SLACK;                                                                           // x axis reference for the point (0,0)

        samples_dist = (float)(width - 2*P_A_SLACK)/max_lenght;                                             // distance between samples (x axis)
        values_dist =  (float)(height - 2*P_A_SLACK)/(float)(2*max_abs_value);                              // distance between values (y axis)

        color(0.91,0.91,0.91);
        rectFill(posx, posy, lim_x, lim_y);                                                                 // background

        color(0,0,0);
        line(posx, posy, lim_x, posy);                                                                      // external rectangle drawing
        line(lim_x, posy, lim_x, lim_y);
        line(lim_x, lim_y, posx, lim_y);
        line(posx, lim_y +1, posx, posy);

        line(x_ref, posy + P_A_SLACK, x_ref, lim_y - P_A_SLACK);                                            // y axis
        for(i = 1; i <= max_abs_value; i+=5){
            line(x_ref-2, y_ref + (i*values_dist),x_ref+2, y_ref + (i*values_dist));
            line(x_ref-2, y_ref - (i*values_dist),x_ref+2, y_ref - (i*values_dist));
        }

        line(x_ref, y_ref, lim_x - P_A_SLACK , y_ref);                                                      // x axis
        for(i = 1; i < max_lenght; i++){
            line(x_ref + (i*samples_dist), y_ref-2, x_ref + (i*samples_dist), y_ref+2);
        }

        text(x_ref - 15, y_ref, "0");
        text(x_ref - 20, y_ref + max_abs_value*values_dist, to_string(max_abs_value).c_str());
        text(x_ref - 30, y_ref - max_abs_value*values_dist, to_string(-1*(int)max_abs_value).c_str());
    }

    void render_samples(){
        /* Plots the loaded samples */
        unsigned int i;
        int x, y, lim_y, y_ref, x_ref;
        float values_dist, samples_dist;

        lim_y = posy + height;
        y_ref = (posy + lim_y)/2;                                                                           // y axis reference for the point (0,0)
        x_ref = posx + P_A_SLACK;                                                                           // x axis reference for the point (0,0)

        samples_dist = (float)(width - 2*P_A_SLACK)/max_lenght;                                             // distance between samples (x axis)
        values_dist =  (float)(height - 2*P_A_SLACK)/(float)(2*max_abs_value);                              // distance between values (y axis)

        color(0,1,0);
        for(i = 0; i < samples.size(); i++){
            x = x_ref + i*samples_dist;
            y = y_ref + samples[i]*values_dist;
            circleFill(x, y, 2, 10);
        }
    }

    void render_rsamples(){
        /* Plots the reconstructed samples */
        unsigned int i;
        int x, y, lim_y, y_ref, x_ref;
        float values_dist, samples_dist;

        lim_y = posy + height;
        y_ref = (posy + lim_y)/2;                                                                           // y axis reference for the point (0,0)
        x_ref = posx + P_A_SLACK;                                                                           // x axis reference for the point (0,0)

        samples_dist = (float)(width - 2*P_A_SLACK)/max_lenght;                                             // distance between samples (x axis)
        values_dist =  (float)(height - 2*P_A_SLACK)/(float)(2*max_abs_value);                              // distance between values (y axis)

        color(0,0,1);
        for(i = 0; i < reconstructed_samples.size(); i++){
            x = x_ref + i*samples_dist;
            y = y_ref + reconstructed_samples[i]*values_dist;
            circleFill(x, y, 2, 10);
        }
    }

    void render_differences(){
        /* Plots the difference between loaded and reconstructed samples */
        unsigned int i;
        int x, lim_y, y_ref, x_ref, samples_y, rsamples_y;
        float values_dist, samples_dist;

        lim_y = posy + height;
        y_ref = (posy + lim_y)/2;                                                                           // y axis reference for the point (0,0)
        x_ref = posx + P_A_SLACK;                                                                           // x axis reference for the point (0,0)

        samples_dist = (float)(width - 2*P_A_SLACK)/max_lenght;                                             // distance between samples (x axis)
        values_dist =  (float)(height - 2*P_A_SLACK)/(float)(2*max_abs_value);                              // distance between values (y axis)

        color(1,0,0);
        for(i = 0; i < samples.size(); i++){
            x = x_ref + i*samples_dist;
            samples_y = y_ref + samples.at(i)*values_dist;
            rsamples_y = y_ref + reconstructed_samples[i]*values_dist;

            line(x, samples_y, x, rsamples_y);
        }
    }

    void mouse_check(int button, int state, int x, int y) {}                                                // PlotAreas don't react to mouse clicks
};
