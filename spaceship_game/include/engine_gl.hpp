/*
Arquivo contendo a classe Engine, que representa o motor em si. O motor � composto por 5 unidades distintas: virabrequim, bielas, pist�es e camisas, sendo
composto no total por 1 virabrequim, 4 pist�es, 4 bielas e 4 camisas. Graficamente, cada uma dessas unidades � composta pelas primitivas Cuboid e/ou Cylinder (shapes3D_gl.hpp)

O virabrequim rotaciona de acordo com um RPM (que pode ser definido) e um FPS (obtido). Essa rota��o causa movimento da biela e dos pist�es, sendo o movimento
desses dois ultimos definidos a partir de rela��es trigonometricas com o angulo atual do virabrequim em torno do eixo x.

As transforma��es gr�ficas s�o implementadas utilizando OpenGL.

Autor: Victor O. Costa
*/

#ifndef ENGINE_H
#define ENGINE_H

#include <GL/glut.h>
#include <GL/freeglut_ext.h>
#include <math.h>
#include <time.h>
#include <to_str.hpp>

#include <transforms3D.hpp>
#include <shapes3D_gl.hpp>
#include <frames.h>

#ifndef PI
#define PI      3.14159265358979323846
#endif

class Engine
{
private:
    Cuboid *vrb_cub[8];                         // (virabrequim) cuboids
    Cylinder *vrb_cyl[9];                       // (virabrequim) cylinders
    bool vrb_visibility;

    Cuboid *bie_cub[4];                         // (biela) cuboids
    bool bie_visibility;

    Cylinder *pis_cyl[4];                       // (pistao) cylinders
    bool pis_visibility;

    Cylinder *cam_cyl[4];                       // (camisa) cylinders
    bool cam_visibility;

    bool persp_projection;                      // if true, perspective projection, else, orthographic

    Point3Dd camera_eye;                        // the camera eye according to the lookat camera setting
    Point3Dd camera_target;                     // the camera target according to the lookat camera
    float camera_theta;                         // the camera theta angle in spherical coordinates
    float camera_phi;                           // the camera phi angle in spherical coordinates

    float alpha;                                // determines the transparency of the engine
    float zoom_out;                             // defines the radius of the camera eye in spherical coordinates
    float angle_x;                              // the x axis angle, used for the engine to work
    float angle_increment;                      // the increment of angle_x at each call of run()
    float RPM;                                  // engine's RPM
    Frames frm;
    int window_height, window_width;            // window size to calculate screen coordinates
    bool holding_click;                         // indicates wether there's a click being holded or not

public:
    // Constructor
    Engine(int win_w, int win_h)
    {
        int i;

        window_height = win_h;
        window_width = win_w;

        // initial settings
        persp_projection = true;
        holding_click = false;
        angle_x = 0;
        RPM = 120;
        alpha = 0.5;

        // set initial camera
        camera_target.x = 0;    camera_target.y = 0;    camera_target.z = 0;
        camera_theta = 0;
        camera_phi = PI/2;
        zoom_out = 200;
        camera_eye_update(camera_theta, camera_phi, zoom_out);

        vrb_visibility = false;
        bie_visibility = false;
        pis_visibility = false;
        cam_visibility = false;

        // (virabrequim) geometric definition
        vrb_cub[0] = new Cuboid(-44,-20,40,4,10);
        vrb_cub[1] = new Cuboid(-32,-20,26,4,10);
        vrb_cub[2] = new Cuboid(-20,-6,26,4,10);
        vrb_cub[3] = new Cuboid(-8,-20,40,4,10);
        vrb_cub[4] = new Cuboid(4,-20,40,4,10);
        vrb_cub[5] = new Cuboid(16,-6,26,4,10);
        vrb_cub[6] = new Cuboid(28,-20,26,4,10);
        vrb_cub[7] = new Cuboid(40,-20,40,4,10);

        vrb_cyl[0] = new Cylinder(-52,0,8,3);
        vrb_cyl[1] = new Cylinder(-40,-14,8,3);
        vrb_cyl[2] = new Cylinder(-28,0,8,3);
        vrb_cyl[3] = new Cylinder(-16,14,8,3);
        vrb_cyl[4] = new Cylinder(-4,0,8,3);
        vrb_cyl[5] = new Cylinder(8,14,8,3);
        vrb_cyl[6] = new Cylinder(20,0,8,3);
        vrb_cyl[7] = new Cylinder(32,-14,8,3);
        vrb_cyl[8] = new Cylinder(44,0,8,3);

        // (biela) geometric definition
        bie_cub[0] = new Cuboid(-38,-6,50,4,8);
        bie_cub[1] = new Cuboid(-14,-6,50,4,8);
        bie_cub[2] = new Cuboid(10,-6,50,4,8);
        bie_cub[3] = new Cuboid(34,-6,50,4,8);

        // (pistao) geometric definition
        pis_cyl[0] = new Cylinder(0,0,5,5);
        pis_cyl[1] = new Cylinder(0,0,5,5);
        pis_cyl[2] = new Cylinder(0,0,5,5);
        pis_cyl[3] = new Cylinder(0,0,5,5);

        for(i=0; i<4; i++)
            pis_cyl[i]->rotate_grid('z',-PI/2);
        pis_cyl[0]->translate_grid(-36,45+3,0);
        pis_cyl[1]->translate_grid(-12,45+3,0);
        pis_cyl[2]->translate_grid(12,45+3,0);
        pis_cyl[3]->translate_grid(36,45+3,0);

        // (camisa) geometric definition
        cam_cyl[0] = new Cylinder(0,0,22,8);
        cam_cyl[1] = new Cylinder(0,0,22,8);
        cam_cyl[2] = new Cylinder(0,0,22,8);
        cam_cyl[3] = new Cylinder(0,0,22,8);

        for(i=0; i<4; i++){
            cam_cyl[i]->rotate_grid('z',PI/2);
            cam_cyl[i]->set_covered(false);
        }
        cam_cyl[0]->translate_grid(-36,40,0);
        cam_cyl[1]->translate_grid(-12,40,0);
        cam_cyl[2]->translate_grid(12,40,0);
        cam_cyl[3]->translate_grid(36,40,0);
    }

    /* Defines the increment of the angle_x at each run call, based on both FPS and RPM */
    void increment_angle_x(){
        float FPS;

        FPS = frm.getFrames();
        angle_increment = (2*PI*RPM)/(60*FPS);

        if(angle_x < 2*PI)
            angle_x += angle_increment;
        else
            angle_x = 0;
    }

    /* Defines the projection to be applied to the scene */
    void project(){
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity ();

        if(persp_projection)
            gluPerspective(60, (GLfloat)window_width/(GLfloat)window_height, 1.0, 400.0);
        else
            glOrtho(-zoom_out,zoom_out,-zoom_out,zoom_out, 1 , 400);

        glMatrixMode (GL_MODELVIEW);
    }

    /* Puts the engine to work, calculating needed angles and applying all sorts of transforms (from model to viewport coordinates) */
    void run(){
        int i;
        float y_bie03, y_bie12;                                                     // y translation of the (biela)
        float x, biela_angle;                                                       // x = distance from (pistao) to rotation center
        float dg_angle_x, dg_biela_angle;                                           // (biela) angle in degrees

        // increments the x axis angle according to FPS and RPM
        increment_angle_x();

        // (biela) angle derived from the law of cosines
        x = 14*cos(angle_x) + sqrt(1936 - 196*pow(sin(angle_x),2));
        biela_angle = acos((1740 + pow(x,2))/(88*x));
        if(angle_x >= PI)
            biela_angle = -1*biela_angle;

        // rad to degrees conversion
        dg_angle_x = angle_x*180/PI;
        dg_biela_angle = biela_angle*180/PI;

        // definition of the view transform, to be applied to every vertex
        glLoadIdentity();
        gluLookAt((GLdouble)camera_eye.x,    (GLdouble)camera_eye.y,    (GLdouble)camera_eye.z,                 // eye
                 (GLdouble)camera_target.x, (GLdouble)camera_target.y, (GLdouble)camera_target.z,               // target
                  0, 1, 0);                                                                                     // up

        // MODEL TRANSFORMS
        // defines (virabrequim) model transform and renders it
        glPushMatrix();
        glRotatef(dg_angle_x, 1.0, 0.0, 0.0);
        if(vrb_visibility)
        {
            //glColor3f(0.0,0.0,0.0);
            glColor4f(0.5, 0.5, 0.5, alpha);
            for(i=0; i<8; i++)
                vrb_cub[i]->render();
            for(i=0; i<9; i++)
                vrb_cyl[i]->render();
        }
        glPopMatrix();


        // defines first and last (biela) cuboids model transform and renders it
        y_bie03 = - 14*cos(angle_x);                                    // (biela) vertical movement
        glPushMatrix();
        glTranslatef(0, y_bie03, 0);
        glTranslatef(0, 44, 0);
        glRotatef(dg_biela_angle, 1.0, 0.0, 0.0);
        glTranslatef(0, -44, 0);
        if(bie_visibility)
        {
            glColor4f(1.0, 0.5, 0.5, alpha);
            bie_cub[0]->render();
            bie_cub[3]->render();
        }
        glPopMatrix();

        // defines second and third (biela) cuboids model transform and renders it
        y_bie12 = 14*cos(angle_x);                                      // (biela) vertical movement
        glPushMatrix();
        glTranslatef(0, y_bie12, 0);
        glTranslatef(0, 44, 0);
        glRotatef(-dg_biela_angle, 1.0, 0.0, 0.0);
        glTranslatef(0, -44, 0);
        if(bie_visibility)
        {
            glColor4f(1.0, 0.5, 0.5, alpha);
            bie_cub[1]->render();
            bie_cub[2]->render();
        }
        glPopMatrix();


        // defines first and last (pistao) cylinders model transform and renders it
        glPushMatrix();
        glTranslatef(0, y_bie03, 0);
        if(pis_visibility)
        {
            glColor4f(0.5, 1.0, 0.5, alpha);
            pis_cyl[0]->render();
            pis_cyl[3]->render();
        }
        glPopMatrix();

        // defines second and third (pistao) cylinders model transform and renders it
        glPushMatrix();
        glTranslatef(0, y_bie12, 0);
        if(pis_visibility)
        {
            glColor4f(0.5, 1.0, 0.5, alpha);
            pis_cyl[1]->render();
            pis_cyl[2]->render();
        }
        glPopMatrix();


        // renders (camisa)
        glPushMatrix();
        if(cam_visibility)
        {
            glColor4f(0.5, 0.5, 1.0, alpha);
            for(i=0; i<4; i++)
                cam_cyl[i]->render();
        }
        glPopMatrix();
    }

    /* Converts camera eye corrdinates from spherical to cartesian */
    void camera_eye_update(float theta, float phi, float r){
        // GL x axis = spherical y axis
        // GL y axis = spherical z axis
        // GL z axis = spherical x axis
        camera_eye.x = r*sin(theta)*sin(phi);
        camera_eye.y = r*cos(phi);
        camera_eye.z = r*cos(theta)*sin(phi);
    }

    /* Checks for mouse wheel actions to update the camera eye radius in spherical coordintes (zoom_out) */
    void wheel_check(int wheel, int direction, int x, int y){
        if(direction == 1){
            if(zoom_out > 100)
                zoom_out -= 10;
        }
        else{
            if(zoom_out < 400)
                zoom_out += 10;
        }

        camera_eye_update(camera_theta, camera_phi, zoom_out);
    }

    /* Checks for mouse activities (clicks and movement) to update the camera eye */
    void mouse_check(int button, int state, int x, int y){
        static int recorded_x, recorded_y;
        static float temp_theta, temp_phi;
        if(x > 60 && x < 1220){
            if(button == GLUT_LEFT_BUTTON){                                                                 // if left mouse button
                switch(state){
                    case GLUT_DOWN:                                                                         // if mouse click
                        recorded_x = x;
                        recorded_y = y;
                        holding_click = true;
                    break;

                    case GLUT_UP:                                                                           // if mouse release
                        holding_click = false;
                        if((recorded_x - x != 0) || (recorded_y - y != 0)){                                 // if mouse was dragged while clicked
                            camera_theta = temp_theta;
                            camera_phi = temp_phi;
                        }
                    break;
                }
            }
        }
        if(button == -1 && holding_click){
            temp_theta = camera_theta + (2*PI/window_width)*(recorded_x - x);
            temp_phi = camera_phi + (PI/(3*window_height))*(y - recorded_y);

            if(temp_phi > 2*PI/3)
                temp_phi = 2*PI/3;
            if(temp_phi < PI/3)
                temp_phi = PI/3;
            camera_eye_update(temp_theta, temp_phi, zoom_out);
        }
    }

    /* Defines if filled or wireframe */
    void set_fill(const char *selected){
        bool value;
        int i;
        if(!strcmp(selected, "PREENCHIMENTO"))
            value = true;
        else
            value = false;

        for(i = 0; i < 8; i++){
            vrb_cub[i]->set_fill(value);
            vrb_cyl[i]->set_fill(value);
        }

        for(i = 0; i < 4; i++){
            bie_cub[i]->set_fill(value);
            pis_cyl[i]->set_fill(value);
            cam_cyl[i]->set_fill(value);
        }
    }

    /* Sets the projection type */
    void set_projection(const char *type){
        if(!strcmp(type,"PERSPECTIVA"))
            persp_projection = true;
        else
            persp_projection = false;
    }

    /* Sets the engine's RPM based on the current RPM and FPS */
    void set_RPM(float rpm){
        RPM = rpm;
    }

    /* Sets the transparency of the engine [0,1] */
    void set_alpha(float value){
        if(value > 1)                                   // value validation
            alpha = 1;
        else if (value < 0)
            alpha = 0;
        else
            alpha = value;
    }

    /* Setters to the visibility of the engine's components */
    void set_vrb_visibility(bool status){
        vrb_visibility = status;
    }

    void set_bie_visibility(bool status){
        bie_visibility = status;
    }

    void set_pis_visibility(bool status){
        pis_visibility = status;
    }

    void set_cam_visibility(bool status){
        cam_visibility = status;
    }

};


#endif      // ENGINE_H
