/*
Arquivo contendo funções para encontrar vetores normais de superfície
Author: Victor O. Costa
*/

#ifndef VEC_OPS_H
#define VEC_OPS_H

#include <point.h>
#include <math.h>

/* Calculates the cross product between two vectors */
Point3Df cross_product(Point3Df vec1, Point3Df vec2){
    Point3Df result;

    result.x = vec1.y * vec2.z - vec1.z * vec2.y;
    result.y = vec1.z * vec2.x - vec1.x * vec2.z;
    result.z = vec1.x * vec2.y - vec1.y * vec2.x;

    return result;
}

/* Normalize a given vector */
Point3Df normalize(Point3Df vec){
    float magnitude;
    Point3Df normalized_vec;

    magnitude = sqrt(pow(vec.x, 2) + pow(vec.y, 2)+ pow(vec.z, 2));
    normalized_vec.x = vec.x/magnitude;
    normalized_vec.y = vec.y/magnitude;
    normalized_vec.z = vec.z/magnitude;

    return normalized_vec;
}

/* Calculates surface normal for a given triangle */
Point3Df surface_normal(Point3Df vertex1, Point3Df vertex2, Point3Df vertex3){
    Point3Df edge1_vec, edge2_vec;
    Point3Df cross;

    edge1_vec.x = vertex2.x - vertex1.x;    edge1_vec.y = vertex2.y - vertex1.y;    edge1_vec.z = vertex2.z - vertex1.z;
    edge2_vec.x = vertex3.x - vertex1.x;    edge2_vec.y = vertex3.y - vertex1.y;    edge2_vec.z = vertex3.z - vertex1.z;
    cross = cross_product(edge1_vec, edge2_vec);
    cross = normalize(cross);

    return cross;
}

#endif
