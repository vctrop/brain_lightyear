/*
Arquivo contendo as primitivas 3D cuboid e cylinder. Cada uma das primitivas tem seus v�rtices definidos atrav�s de uma malha de pontos 3D (grid).
Essas primitivas podem ser visualizadas em forma preenchida ou wireframe, utilizando OpenGL para renderiza��o.

Autor: Victor O. Costa
*/

#ifndef SHAPES3D_H
#define SHAPES3D_H

#include <math.h>
#include <point.h>

#include <vec_operations.h>
#include <transforms3D.hpp>

#include <GL/glut.h>
#include <GL/freeglut_ext.h>

#define PI 3.14159265359
#define CY_NUM_CIRCLES 30
#define CY_NUM_POINTS 40
#define CU_NUM_RECTS 40

class Cuboid
{
private:
    Point3Df grid[4][CU_NUM_RECTS];                            // original origin centered coordinates

    float height, rect_len, rect_wid;
    float angle_x, angle_y, angle_z;
    bool fill;

public:
    Cuboid(int x_pos, int y_pos, float hei, float len, float wid){
        int rect, rect_vert;                                    // rectangle and rectangle vertex

        height = hei;
        rect_len = len;
        rect_wid = wid;
        fill = true;

        angle_x = 0; angle_y = 0; angle_z = 0;

        // cuboid definition arround the y axis, from 0 to h
        // vertex 0 is the one where x and z are both positive, and the other vertices follow clockwise
        for(rect = 0; rect < CU_NUM_RECTS; rect++)
            for(rect_vert = 0; rect_vert < 4; rect_vert++){
                grid[rect_vert][rect].y = rect*(height/(CU_NUM_RECTS-1));  // y coordinate only depends on 'rect'

                // x and z coordinates depend on the vertex
                if(rect_vert == 0 || rect_vert == 3)                // positive x
                    grid[rect_vert][rect].x = rect_len/2;
                else                                                // negative x
                    grid[rect_vert][rect].x = -rect_len/2;

                if(rect_vert == 0 || rect_vert == 1)                // positive z
                    grid[rect_vert][rect].z = rect_wid/2;
                else                                                // negative z
                    grid[rect_vert][rect].z = -rect_wid/2;
            }

        // moves x and y coords to (x_pos,y_pos). In the end, the center of (the left side of the lowest rect will be in x,y pos. z isn't affected.)
        for(rect = 0; rect < CU_NUM_RECTS; rect++)
            for(rect_vert = 0; rect_vert < 4; rect_vert++){
                grid[rect_vert][rect].x += x_pos + rect_len/2;
                grid[rect_vert][rect].y += y_pos;
                //grid[rect_vert][rect].x += x_pos;
                //grid[rect_vert][rect].y -= + y_pos;
            }
    }

    void render(){
        int rect, rect_vert;                                    // rectangle and rectangle vertex
        Point3Df normal_vec;

        if(fill){                                                       // solid cuboid
            glBegin(GL_TRIANGLES);
            for(rect = 0; rect < CU_NUM_RECTS; rect++){
                for(rect_vert = 0; rect_vert < 4; rect_vert++){
                    // draw sides
                    if(rect != CU_NUM_RECTS - 1){
                        // First triangle
                        normal_vec = surface_normal(grid[rect_vert][rect], grid[(rect_vert+1)%4][rect+1], grid[rect_vert][rect+1]);
                        glNormal3f(normal_vec.x, normal_vec.y, normal_vec.z);
                        glVertex3f(grid[rect_vert][rect].x, grid[rect_vert][rect].y, grid[rect_vert][rect].z);
                        glVertex3f(grid[rect_vert][rect+1].x, grid[rect_vert][rect+1].y, grid[rect_vert][rect+1].z);
                        glVertex3f(grid[(rect_vert+1)%4][rect+1].x, grid[(rect_vert+1)%4][rect+1].y, grid[(rect_vert+1)%4][rect+1].z);

                        // Second triangle
                        normal_vec = surface_normal(grid[rect_vert][rect], grid[(rect_vert+1)%4][rect], grid[(rect_vert+1)%4][rect+1]);
                        glNormal3f(normal_vec.x, normal_vec.y, normal_vec.z);
                        glVertex3f(grid[rect_vert][rect].x, grid[rect_vert][rect].y, grid[rect_vert][rect].z);
                        glVertex3f(grid[(rect_vert+1)%4][rect].x, grid[(rect_vert+1)%4][rect].y, grid[(rect_vert+1)%4][rect].z);
                        glVertex3f(grid[(rect_vert+1)%4][rect+1].x, grid[(rect_vert+1)%4][rect+1].y, grid[(rect_vert+1)%4][rect+1].z);
                    }
                }
                // draw cover
                if(rect == 0 || rect == CU_NUM_RECTS - 1){
                    // First triangle
                    if(rect == 0)
                        normal_vec = surface_normal(grid[0][rect], grid[1][rect], grid[2][rect]);
                    else
                        normal_vec = surface_normal(grid[0][rect], grid[2][rect], grid[1][rect]);

                    glNormal3f(normal_vec.x, normal_vec.y, normal_vec.z);
                    glVertex3f(grid[0][rect].x, grid[0][rect].y, grid[0][rect].z);
                    glVertex3f(grid[1][rect].x, grid[1][rect].y, grid[1][rect].z);
                    glVertex3f(grid[2][rect].x, grid[2][rect].y, grid[2][rect].z);

                    // Second triangle
                    if(rect == 0)
                        normal_vec = surface_normal(grid[0][rect], grid[2][rect], grid[3][rect]);
                    else
                        normal_vec = surface_normal(grid[0][rect], grid[3][rect], grid[2][rect]);

                    glNormal3f(normal_vec.x, normal_vec.y, normal_vec.z);
                    glVertex3f(grid[0][rect].x, grid[0][rect].y, grid[0][rect].z);
                    glVertex3f(grid[2][rect].x, grid[2][rect].y, grid[2][rect].z);
                    glVertex3f(grid[3][rect].x, grid[3][rect].y, grid[3][rect].z);

                }
            }
            glEnd();
        }
        else{                                                           // wireframe
            glPointSize(2);
            // render vertices
            glBegin(GL_POINTS);
            for(rect = 0; rect < CU_NUM_RECTS; rect++)
                for(rect_vert = 0; rect_vert < 4; rect_vert++)
                    glVertex3f(grid[rect_vert][rect]. x,grid[rect_vert][rect].y, grid[rect_vert][rect].z);
            glEnd();

            // render edges
            glBegin(GL_LINES);
            for(rect = 0; rect < CU_NUM_RECTS; rect++){
                for(rect_vert = 0; rect_vert < 4; rect_vert++){
                    // connects the vertices of a rect and decides the next vertex of the diagonal edge
                    glVertex3f(grid[rect_vert][rect].x, grid[rect_vert][rect].y, grid[rect_vert][rect].z);
                    glVertex3f(grid[(rect_vert+1)%4][rect].x, grid[(rect_vert+1)%4][rect].y, grid[(rect_vert+1)%4][rect].z);

                    // connects the rectangles
                    if(rect != CU_NUM_RECTS - 1){
                        // perpendicular
                        glVertex3f(grid[rect_vert][rect].x, grid[rect_vert][rect].y, grid[rect_vert][rect].z);
                        glVertex3f(grid[rect_vert][rect+1].x, grid[rect_vert][rect+1].y, grid[rect_vert][rect+1].z);

                        //diagonal
                        glVertex3f(grid[rect_vert][rect].x, grid[rect_vert][rect].y, grid[rect_vert][rect].z);
                        glVertex3f(grid[(rect_vert + 1)%4][rect+1].x, grid[(rect_vert + 1)%4][rect+1].y, grid[(rect_vert + 1)%4][rect+1].z);
                    }
                }

                // cover
                if(rect == 0 || rect == CU_NUM_RECTS - 1){
                    glVertex3f(grid[0][rect].x, grid[0][rect].y, grid[0][rect].z);
                    glVertex3f(grid[2][2].x, grid[2][rect].y, grid[2][rect].z);

                    glVertex3f(grid[1][rect].x, grid[1][rect].y, grid[1][rect].z);
                    glVertex3f(grid[3][rect].x, grid[3][rect].y, grid[3][rect].z);
                }
            }
            glEnd();
        }
    }

    void set_fill(bool value){
        fill = value;
    }

};

class Cylinder
{
private:
    Point3Df grid[CY_NUM_POINTS][CY_NUM_CIRCLES];                // original origin centered coordinates
    Transform_3D current_transform;

    float lenght, radius;
    float angle_x, angle_y, angle_z;
    bool fill;
    bool covered;

public:
    Cylinder(int x_pos, int y_pos, float len, float rad){
        float circle_angle;
        int circle, circ_point;

        fill = true;
        covered = true;
        lenght = len;
        radius = rad;
        angle_x = 0; angle_y = 0; angle_z = 0;

        // creates NUM_CIRCLE circles, each with NUM_POINT vertices
        // cylinder arround the x axis, from 0 to lenght
        for(circle = 0; circle < CY_NUM_CIRCLES; circle++)
            for(circ_point = 0; circ_point < CY_NUM_POINTS; circ_point++){
                circle_angle = (2*PI/CY_NUM_POINTS)*circ_point;
                grid[circ_point][circle].x = circle * (lenght/(CY_NUM_CIRCLES-1));
                grid[circ_point][circle].y = radius*sin(circle_angle);
                grid[circ_point][circle].z = radius*cos(circle_angle);
            }

        // moves x and y coords to (x_pos,y_pos). In the end, the center of the leftmost circle will be in x,y pos. z isn't affected.
        for(circle = 0; circle < CY_NUM_CIRCLES; circle++)
            for(circ_point = 0; circ_point < CY_NUM_POINTS; circ_point++){
                grid[circ_point][circle].x += x_pos;
                grid[circ_point][circle].y += y_pos;
            }
    }

    void render(){
        int circle, circ_point;
        Point3Df normal_vec;
        float x_sum_fst = 0, y_sum_fst = 0, z_sum_fst = 0;                                      // auxiliar variables to find the first circle center
        float x_sum_lst = 0, y_sum_lst = 0, z_sum_lst = 0;                                      // auxiliar variables to find the last circle center
        Point3Df circle_center;                                                                 // center of the circle;

        if(fill){                                                                   // solid cylinder
            for(circle=0; circle<CY_NUM_CIRCLES; circle++){
                glBegin(GL_TRIANGLES);
                for(circ_point=0; circ_point<CY_NUM_POINTS; circ_point++){
                    if(circle != CY_NUM_CIRCLES - 1){
                        // First triangle
                        normal_vec = surface_normal(grid[circ_point][circle], grid[(circ_point+1)%CY_NUM_POINTS][circle+1], grid[circ_point][circle+1]);
                        glNormal3f(normal_vec.x, normal_vec.y, normal_vec.z);
                        glVertex3f(grid[circ_point][circle].x, grid[circ_point][circle].y, grid[circ_point][circle].z);
                        glVertex3f(grid[circ_point][circle+1].x, grid[circ_point][circle+1].y, grid[circ_point][circle+1].z);
                        glVertex3f(grid[(circ_point+1)%CY_NUM_POINTS][circle+1].x, grid[(circ_point+1)%CY_NUM_POINTS][circle+1].y, grid[(circ_point+1)%CY_NUM_POINTS][circle+1].z);

                        // Second triangle
                        normal_vec = surface_normal(grid[circ_point][circle], grid[(circ_point+1)%CY_NUM_POINTS][circle], grid[(circ_point+1)%CY_NUM_POINTS][circle+1]);
                        glNormal3f(normal_vec.x, normal_vec.y, normal_vec.z);
                        glVertex3f(grid[circ_point][circle].x, grid[circ_point][circle].y, grid[circ_point][circle].z);
                        glVertex3f(grid[(circ_point+1)%CY_NUM_POINTS][circle].x, grid[(circ_point+1)%CY_NUM_POINTS][circle].y, grid[(circ_point+1)%CY_NUM_POINTS][circle].z);
                        glVertex3f(grid[(circ_point+1)%CY_NUM_POINTS][circle+1].x, grid[(circ_point+1)%CY_NUM_POINTS][circle+1].y, grid[(circ_point+1)%CY_NUM_POINTS][circle+1].z);
                    }

                    // get coordinates to calc the middle of the first and last circles
                    if(circle == 0){
                        x_sum_fst += grid[circ_point][circle].x;
                        y_sum_fst += grid[circ_point][circle].y;
                        z_sum_fst += grid[circ_point][circle].z;
                    }
                    else if(circle == CY_NUM_CIRCLES - 1){
                        x_sum_lst += grid[circ_point][circle].x;
                        y_sum_lst += grid[circ_point][circle].y;
                        z_sum_lst += grid[circ_point][circle].z;
                    }
                }
                glEnd();

                // draw cover
                if(covered && (circle == 0 || circle == CY_NUM_CIRCLES - 1)){
                    // cover one
                    if(circle == 0){
                        circle_center.x = x_sum_fst/CY_NUM_POINTS;
                        circle_center.y = y_sum_fst/CY_NUM_POINTS;
                        circle_center.z = z_sum_fst/CY_NUM_POINTS;
                        x_sum_fst = y_sum_fst = z_sum_fst = 0;
                    }
                    // cover two
                    else{
                        circle_center.x = x_sum_lst/CY_NUM_POINTS;
                        circle_center.y = y_sum_lst/CY_NUM_POINTS;
                        circle_center.z = z_sum_lst/CY_NUM_POINTS;
                        x_sum_lst = y_sum_lst = z_sum_lst = 0;
                    }

                    glBegin(GL_TRIANGLE_FAN);
                        // surface normal
                        normal_vec = surface_normal(circle_center, grid[0][circle], grid[1][circle]);
                        glNormal3f(normal_vec.x, normal_vec.y, normal_vec.z);

                        glVertex3f(circle_center.x, circle_center.y, circle_center.z);
                        for(circ_point=0; circ_point<CY_NUM_POINTS; circ_point++){
                            glVertex3f(grid[circ_point][circle].x, grid[circ_point][circle].y, grid[circ_point][circle].z);
                        }
                        glVertex3f(grid[0][circle].x, grid[0][circle].y, grid[0][circle].z);
                    glEnd();
                }
            }

        }
        else{                                                                       // wireframe
            glPointSize(2);
            // render vertices
            glBegin(GL_POINTS);
            for(circle = 0; circle < CY_NUM_CIRCLES; circle++)
                for(circ_point = 0; circ_point < CY_NUM_POINTS; circ_point++)
                    glVertex3f(grid[circ_point][circle].x, grid[circ_point][circle].y, grid[circ_point][circle].z);
            glEnd();

            // render edges
            glBegin(GL_LINES);
            for(circle=0; circle<CY_NUM_CIRCLES; circle++){
                for(circ_point=0; circ_point<CY_NUM_POINTS; circ_point++){
                    // connects the vertices of a circle
                    glVertex3f(grid[circ_point][circle].x, grid[circ_point][circle].y,grid[circ_point][circle].z);
                    glVertex3f(grid[(circ_point+1)%CY_NUM_POINTS][circle].x, grid[(circ_point+1)%CY_NUM_POINTS][circle].y,grid[(circ_point+1)%CY_NUM_POINTS][circle].z);

                    // connects the circles
                    if(circle != CY_NUM_CIRCLES - 1){
                        // perpendicular
                        glVertex3f(grid[circ_point][circle].x, grid[circ_point][circle].y, grid[circ_point][circle].z);
                        glVertex3f(grid[circ_point][circle+1].x, grid[circ_point][circle+1].y, grid[circ_point][circle+1].z);

                        // diagonal
                        glVertex3f(grid[circ_point][circle].x, grid[circ_point][circle].y, grid[circ_point][circle].z);
                        glVertex3f(grid[(circ_point+1)%CY_NUM_POINTS][circle+1].x, grid[(circ_point+1)%CY_NUM_POINTS][circle+1].y, grid[(circ_point+1)%CY_NUM_POINTS][circle+1].z);
                    }
                }
            }
            glEnd();
        }
    }

    void set_fill(bool value){
        fill = value;
    }

    void set_covered(bool value){
        covered = value;
    }

    /* Translate the grid itself */
    void translate_grid(float tx, float ty, float tz){
        int circle, circ_point;

        current_transform.concat_translation(tx,ty,tz);
        for(circle = 0; circle < CY_NUM_CIRCLES; circle++)
            for(circ_point = 0; circ_point < CY_NUM_POINTS; circ_point++)
                grid[circ_point][circle] = current_transform * grid[circ_point][circle];
        current_transform.load_identity();
    }

    /* Rotate the grid itself */
    void rotate_grid(char axis, float angle_rad){
        int circle, circ_point;

        current_transform.concat_rotation(axis, angle_rad);
        for(circle = 0; circle < CY_NUM_CIRCLES; circle++)
            for(circ_point = 0; circ_point < CY_NUM_POINTS; circ_point++)
                grid[circ_point][circle] = current_transform * grid[circ_point][circle];
        current_transform.load_identity();
    }
};


#endif
