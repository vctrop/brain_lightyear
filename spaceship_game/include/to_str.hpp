/* Arquivo contendo patch do MinGw para consertar o método to_str */
#ifndef TO_STR_PATCH
#define TO_STR_PATCH

#include <string>
#include <sstream>
#include <iostream>

using std::string;

// MinGw patch to fix to_string method
namespace patch
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}

#endif
